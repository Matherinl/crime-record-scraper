﻿using System;
using System.Diagnostics;

namespace CrimeRecordCore.Utilites {

	public static class DebuggerUtils {

		///<summary>Will force the debbugger to break if it is attached.</summary>
		public static void BreakIfDebuggerAttached() {
			if(Debugger.IsAttached) {
				Debugger.Break();
			}
		}
	
	}

}
