﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace CrimeRecordCore.Utilites {

	///<summary>A class with helpful utilites when dealing with webcalls and HttpClient.</summary>
	public static class HttpUtils {

		///<summary>Sends a GET request to the given url. The cancellation token will be passed into the HttpClient to cancel it if needed. If the task is cancelled,
		///will return null.</summary>
		///<exception cref="HttpRequestException">Will throw if the web call fails.</exception>
		public static async Task<HttpResponseMessage> GetResponseAsync(string url,CancellationToken token) {
			using HttpClient client=new HttpClient();
			HttpResponseMessage response=null;
			try {
				response=await client.GetAsync(url,token);
			}
			catch(OperationCanceledException) {
				//If they cancelled us, we will return null.
			}
			return response;
		}

	}

}
