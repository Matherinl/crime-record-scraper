﻿namespace CrimeRecordCore.Enums {

	public enum Race {
		White,
		Black,
		Hispanic,
		Asian,
		PacificIslander,
		Other,
		NativeAmerican,
	}

}
