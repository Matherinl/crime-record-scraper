﻿
using System.ComponentModel;

namespace CrimeRecordCore.Enums {

	///<summary>The type of charges we record.</summary>
	public enum ChargeType {
		//Unable to determine the charge type.
		Unknown,
		[Description("Disorderly Conduct")]
		DisorderlyConduct,
		[Description("Attempt to Commit a Crime")]
		AttemptToCommitCrime,
		[Description("Violation Of Release Agreement")]
		ViolationOfReleaseAgreement,
		Trespassing,
		[Description("Fail to Appear")]
		FailToAppear,
		[Description("Probation Violation")]
		ProbationViolation,
		[Description("Parole Violation")]
		ParoleViolation,
		[Description("Assault of a Police Officer")]
		AssaultOfPoliceOfficer,
		[Description("Aggravated Harassment")]
		AggravatedHarassment,
		Theft,
		Robbery,
		[Description("Fraudulent Use of a Credit Cards")]
		FraudulentUseOfACreditCard,
		Fugitive,
		[Description("Sexual Abuse")]
		SexualAbuse,
		Strangulation,
		Sodomy,
		[Description("Sexual Penetration")]
		SexualPenetration,
		[Description("Unlawful Distribution of a Controlled Substance")]
		UnlawfulDistributionOfControlledSubstance,
		[Description("Giving Liquor to a Minor")]
		GivingLiquorToMinor,
		[Description("Possesion of a Weapon")]
		PossesionOfAWeapon,
		[Description("Driving with a Suspended Lincese")]
		DrivingWithSuspendedLicense,
		Assault,
		[Description("Unautharized Use of a Motor Vehicle")]
		UnautharizedUseOfMotorVehicle,
		Rape,
		[Description("Hold for Charge")]
		HoldForCharge,
		[Description("Violation of a Restraining Order")]
		ViolatingRestrainingOrder,
		[Description("Resisting Arrest")]
		ResistingArrest,
		Methamphetamine,
		Kidnapping,
		Coercion,
		Oxycodone,
		[Description("Contempt of Court")]
		ContemptOfCourt,
		Heroin,
		Manslaughter,
		Harassment,
		[Description("Driving Under the Influence")]
		DrivingUnderInfluence,
		Burglary,
		Arson,
		[Description("Possesion of a Stolen Vehicle")]
		PossesionOfAStolenVehicle,
		[Description("Criminal Mischief")]
		CriminalMischief,
		Murder,
		[Description("Unlawful Use of a Weapon")]
		UnlawfulUseOfAWeapon,
		Menacing,
		[Description("Hit and Run")]
		HitAndRun,
		[Description("Reckless Driving")]
		RecklessDriving,
		[Description("Attempt to Elude Police")]
		AttemptToEludePolice,
		[Description("Aggravated Murder")]
		AggravatedMurder,
		[Description("Interfere with Making a Report")]
		InterfereWithMakingReport,
		Stalking,
		[Description("Public Indecency")]
		PublicIndecency,
		[Description("Providing False Info to a Police Officer")]
		FalseInfoToPoliceOfficer,
		[Description("Interfere with Public Transportation")]
		InterfereWithPublicTransportation,
		[Description("Interfering with a Police Officer")]
		InterfereWithPoliceOfficer,
		[Description("Failure to Report as a Sex Offender")]
		FailToReportAsSexOffender,
		[Description("Private Indecency")]
		PrivateIndecency,
		Incest,
		[Description("Encouraging Child Sex Abuse")]
		EncourageChildSexAbuse,
		[Description("Computer Crime")]
		ComputerCrime,
		[Description("Luring a Minor")]
		LuringAMinor,
		Prostitution,
		[Description("Unlawful Purchase of a Firearm")]
		UnlawfulPurchaseOfFirearm,
		Escape,
		[Description("Public Intoxication")]
		PublicIntoxication,
		[Description("Controlled Substance Offense")]
		ControlledSubstanceOffense,
		[Description("Tampering with a Witness")]
		TamperingWithAWitness,
		Racketeering,
		[Description("Reckless Endangerment")]
		RecklessEndangerment,
		[Description("Criminal Mistreatment")]
		CriminalMistreatment,
		Forgery,
	}
}
