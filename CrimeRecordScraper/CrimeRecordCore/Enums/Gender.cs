﻿namespace CrimeRecordCore.Enums {

	public enum Gender {
		Male,
		Female,
		Other,
	}

}
