﻿namespace CrimeRecordCore.TableTypes {

	///<summary>Represent a single county within the United States.</summary>
	public class County {

		///<summary>The primary key of the county.</summary>
		public long Id { get; set; }
		///<summary>The name of the county.</summary>
		public string Name { get; set; }
		///<summary>The abbreviation of the state this county resides in. i.e. OR for Oregon.</summary>
		public string State { get; set; }

		public County() { }

		///<summary>Parameters must match the field names for EF binding to work properly.</summary>
		public County(string name, string state) {
			Name=name;
			State=state;
		}
    }

}
