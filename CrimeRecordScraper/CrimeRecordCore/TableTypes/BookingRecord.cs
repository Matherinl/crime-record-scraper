﻿using System;
using CrimeRecordCore.Enums;

namespace CrimeRecordCore.TableTypes {

	///<summary>Represent a booking record for a single person.</summary>
	public class BookingRecord {

		///<summary>The primary key of the booking record.</summary>
		public long Id { get; set; }
		///<summary>The first name of the person being booked.</summary>
		public string FName { get; set; }
		///<summary>The middle name of the person being booked.</summary>
		public string MName { get; set; }
		///<summary>The last name of the person being booked.</summary>
		public string LName { get; set; }
		///<summary>A foreign key to the county that this person was booked in.</summary>
		public long CountyId { get; set; }
		///<summary>The date that the person being booked was born.</summary>
		public DateTime DateOfBirth { get; set; }
		///<summary>The gender of the person being booked.</summary>
		public Gender Gender { get; set; }
		///<summary>The race of the person being booked.</summary>
		public Race Race { get; set; }
		///<summary>The height of the person being booked in inches.</summary>
		public int Height { get; set; }
		///<summary>The weight of the person being booked in pounds.</summary>
		public int Weight { get; set; }
		///<summary>The date and time this person was booked.</summary>
		public DateTime BookingTime { get; set; }
		///<summary>A link to the person's mugshot. May be blank.</summary>
		public string MugshotImageLink { get; set; }
		///<summary>Indicates if the mugshot stored in <see cref="MugshotImageLink" /> is still valid.</summary>
		public bool IsMugshotValid { get; set; }

	}

}
