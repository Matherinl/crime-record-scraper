﻿namespace CrimeRecordCore.TableTypes {

	///<summary>Represent a link between a booking record and a charge.</summary>
	public class BookingRecordCharge {

		///<summary>The primary key of this BookingRecordCharge.</summary>
		public long Id { get; set; }
		///<summary>A foreign key to the charge.</summary>
		public long ChargeId { get; set; }
		///<summary>A foreign key to the booking record.</summary>
		public long BookingRecordId { get; set; }
		///<summary>The raw charge description.</summary>
		public string ChargeDescriptionRaw { get; set; }

	}

}
