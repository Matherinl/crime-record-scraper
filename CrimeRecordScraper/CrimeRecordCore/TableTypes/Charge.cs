﻿using CrimeRecordCore.Enums;

namespace CrimeRecordCore.TableTypes {

	///<summary>Represent a charge that someone being booked may be charged with.</summary>
	public class Charge {

		///<summary>The primary key of the charge.</summary>
		public long Id { get; set; }
		///<summary>The type of charge the user has.</summary>
		public ChargeType ChargeType { get; set; }

    }

}
