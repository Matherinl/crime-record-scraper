Database Setup:
1. Install Microsoft SQL Server and the Server Management Studio
2. Using the Visual Studio Server Explorer create a new 'Data Connection'. The server name is not as simple as your machine name. Open Server Management Studio
to easily view your server name.
3. Unload the CrimeRecordScraper project. EF Core will always try to run commands from the startup project.
4. Once your data connection is configured you can right-click, view 'Properties', and copy the connection string.
5. Add this connectionstring with a name to DataConnection\connectionstrings.json and call it in DatabaseContext.OnConfiguring()
6. Open the NPM (nuget-package-manager) console and run the command update-database -migration DbSetup-Migration

Updating the Database:
-Do the following if you ever add/change a model or it's fields (including enums).
1. Open NPM and create a new migration script by running 'Add-Migration MigrationName'
2. Run 'Update-Database' to ensure the changes take effect on your local db.
3. Replace the DbSetup_Migration.cs with the new migration you just made and rename it to 'DbSetup_Migration'. 
