﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CrimeRecordCore.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BookingRecordChargeSet",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ChargeId = table.Column<long>(nullable: false),
                    BookingRecordId = table.Column<long>(nullable: false),
                    ChargeDescriptionRaw = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BookingRecordChargeSet", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BookingRecordSet",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FName = table.Column<string>(nullable: true),
                    MName = table.Column<string>(nullable: true),
                    LName = table.Column<string>(nullable: true),
                    CountyId = table.Column<long>(nullable: false),
                    DateOfBirth = table.Column<DateTime>(nullable: false),
                    Gender = table.Column<int>(nullable: false),
                    Race = table.Column<int>(nullable: false),
                    Height = table.Column<int>(nullable: false),
                    Weight = table.Column<int>(nullable: false),
                    BookingTime = table.Column<DateTime>(nullable: false),
                    MugshotImageLink = table.Column<string>(nullable: true),
                    IsMugshotValid = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BookingRecordSet", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ChargeSet",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ChargeType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChargeSet", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CountySet",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    State = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CountySet", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BookingRecordChargeSet");

            migrationBuilder.DropTable(
                name: "BookingRecordSet");

            migrationBuilder.DropTable(
                name: "ChargeSet");

            migrationBuilder.DropTable(
                name: "CountySet");
        }
    }
}
