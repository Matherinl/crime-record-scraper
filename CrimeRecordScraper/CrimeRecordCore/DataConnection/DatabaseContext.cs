﻿using System;
using CrimeRecordCore.DataInterfaces;
using CrimeRecordCore.TableTypes;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.FileExtensions;
using Microsoft.Extensions.Configuration.Json;

namespace CrimeRecordCore.DataConnection {

	public partial class DatabaseContext : DbContext {
		
		public BookingRecordChargesDI BookingRecordCharges { get; set; }
		public BookingRecordsDI BookingRecords { get; set; }
		public ChargesDI Charges { get; set; }
		public CountiesDI Counties { get; set; }

		#region Private DbSets

		private DbSet<BookingRecordCharge> _bookingRecordChargeSet { get; set; }
		private DbSet<BookingRecord> _bookingRecordSet { get; set; }
		private DbSet<Charge> _chargeSet { get; set; }
		private DbSet<County> _countySet { get; set; }

		#endregion

		public DatabaseContext() {
			InitializeDataInterfaces();
		}

		private void InitializeDataInterfaces() {
			BookingRecords=new BookingRecordsDI(this);
			BookingRecordCharges=new BookingRecordChargesDI(this);
			Charges=new ChargesDI(this);
			Counties=new CountiesDI(this);
		}

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
			if(!optionsBuilder.IsConfigured) {
				IConfiguration config=new ConfigurationBuilder().AddJsonFile(@"DataConnection\connectionstrings.json").Build();
				optionsBuilder.UseSqlServer(config["localhost_matherin"]);
			}
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder) {

		}
	}

}
