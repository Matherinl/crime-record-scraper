﻿using System;
using System.Text;

namespace CrimeRecordCore.Extensions {

	public static class StringExtensions {

		///<summary>Returns the string with first character capitalized and the rest lowercase.</summary>
		public static string Capitalize(this string str) {
			if(string.IsNullOrEmpty(str)) {
				return "";
			}
			string retVal=str[0].ToString().ToUpper();
			if(str.Length > 1) {
				retVal+=str.Substring(1).ToLower();
			}
			return retVal;
		}

		///<summary>Will return if the given string contains all of the given substrings.</summary>
		public static bool ContainsAll(this string str,params string[] substrings) {
			if(string.IsNullOrEmpty(str) || substrings==null || substrings.Length==0) {
				return false;
			}
			bool containsAll=true;
			foreach(string substring in substrings) {
				containsAll&=str.Contains(substring);
			}
			return containsAll;
		}

		///<summary>Helper method that parses a name out of the given string that has the format of: 'lastname, firstname middlename'.
		///Returns a string[3] where 0=last name, 1=first name, 2=middle name.</summary>
		private static string[] SetRecordNameFields(string name){
			string[] retVal=new string[3];
			//This will give us [lastname] and [ firstname middlename]
			string[] names=name.Split(',');
			retVal[0]=names[0];
			string[] firstAndMiddle=names[1].Split(' ');
			retVal[1]=firstAndMiddle[1];
			retVal[2]=firstAndMiddle[2];
			return retVal;
		}
	}

}
