﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CrimeRecordCore.DataInterfaces;
using CrimeRecordCore.TableTypes;

namespace CrimeRecordCore.DataConnection {

	public partial class DatabaseContext {

		public class BookingRecordsDI : DataInterfaceAbs<BookingRecord> {

			internal BookingRecordsDI(DatabaseContext context) : base(context) {

			}

		}

	}

}
