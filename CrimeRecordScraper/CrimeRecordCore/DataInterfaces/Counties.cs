﻿using System;
using System.Collections.Generic;
using System.Text;
using CrimeRecordCore.DataConnection;
using CrimeRecordCore.DataInterfaces;
using CrimeRecordCore.TableTypes;

namespace CrimeRecordCore.DataConnection {

	public partial class DatabaseContext {

		public class CountiesDI : DataInterfaceAbs<County> {

			internal CountiesDI(DatabaseContext context) : base(context) {
				
			}

		}

	}

}
