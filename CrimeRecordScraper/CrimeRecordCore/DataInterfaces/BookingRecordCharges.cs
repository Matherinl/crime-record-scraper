﻿using System;
using System.Collections.Generic;
using System.Text;
using CrimeRecordCore.DataInterfaces;
using CrimeRecordCore.TableTypes;

namespace CrimeRecordCore.DataConnection {

	public partial class DatabaseContext {

		public class BookingRecordChargesDI : DataInterfaceAbs<BookingRecordCharge> {

			internal BookingRecordChargesDI(DatabaseContext context) : base(context) {

			}

		}

	}

}
