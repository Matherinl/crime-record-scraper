﻿using System;
using System.Collections.Generic;
using System.Text;
using CrimeRecordCore.DataConnection;

namespace CrimeRecordCore.DataInterfaces {
	public class DataInterfaceAbs<T> where T : class {

		///<summary>The database context for this data interface.</summary>
		protected DatabaseContext _dbContext=null;

		internal DataInterfaceAbs(DatabaseContext context) {
			_dbContext=context;
		}
	}
}
