﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CrimeRecordCore.TableTypes;
using HtmlAgilityPack;

namespace CrimeRecordScraper.Scrapers.Oregon {
	public class MultnomahCounty : CountyScraperAbs{
		protected override string _countyUrlRoot{
			get {
				return "https://www.mcso.us";
			}
		}
		protected override string _bookingUrlPath {	get; set; }
		private List<string> _listRecordLinks;

		public MultnomahCounty(County county) : base(county) {
			//Make a post request to www.mcso.us for bookings in the last 7 days
			using(WebClient wc=new WebClient()) {
				wc.Headers[HttpRequestHeader.ContentType]="application/x-www-form-urlencoded";
				//We could query for additional search types and build even more data. For now the last 7 days will do.
				_bookingUrlPath=wc.UploadString($"{_countyUrlRoot}/PAID/Home/SearchResults","FirstName=&LastName=&SearchType=1");
			}
		}

		private List<string> ReadResultTable() {
			HtmlDocument doc=new HtmlDocument();
			doc.LoadHtml(_bookingUrlPath);
			HtmlNodeCollection nodeTableRowData=doc.DocumentNode.SelectNodes("//table[@class='search-results']/tbody/tr/td");
			if(nodeTableRowData==null || nodeTableRowData.Count==0) {
				//TODO: log
			}
			//Each table row has 2 <td> elements. We only care about the first.
			for(int i=0;i<nodeTableRowData.Count;i+=2) {
				_listRecordLinks.Add(_countyUrlRoot+ParseBookingUrl(nodeTableRowData[i].InnerHtml));
			}
			return _listRecordLinks;
		}

		///<summary>Uses _listRecordLinks to scrape the specific individual data.</summary>
		private void GetIndividualData() {
			foreach(string link in _listRecordLinks) {
				
			}
		}

		///<summary> Attempts to parse the url of the booking details out of the given string using a regex match. Generally regex on HTML is a big no no,
		/// but because this html is small and predictable, and will happen thousands of times the performance gain was deemed necessary. See example below.
		/// Example string format: '<a href="/PAID/Home/Booking/000000/000000">LastName, FirstName MiddleName</a>'</summary>
		private string ParseBookingUrl(string input) {
			//Matches everything between ""
			Regex rx=new Regex("\"([^)]*)\"");
			Match match=rx.Match(input);
			//Remove the leading "
			string retVal=match.Value.Remove(0,1);
			//Remove trailing "
			return retVal.Remove(retVal.Length-1,1);
		}

		protected override async Task RunScraperInternal() {
			_listRecordLinks=ReadResultTable();
			GetIndividualData();
		}
	}
}
