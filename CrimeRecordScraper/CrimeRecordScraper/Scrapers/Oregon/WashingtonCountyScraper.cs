﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrimeRecordCore.Enums;
using CrimeRecordCore.Extensions;
using CrimeRecordCore.TableTypes;
using CrimeRecordCore.Utilites;
using CrimeRecordScraper.Utilities;
using HtmlAgilityPack;

namespace CrimeRecordScraper.Scrapers.Oregon {

	///<summary>A web scraper for Washington county.</summary>
	public class WashingtonCountyScraper : CountyScraperAbs {

		private string[] CapitalLetters={ "A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z" };

		protected override string _countyUrlRoot {
			get {
				return "https://www.co.washington.or.us";
			}
		}

		protected override string _bookingUrlPath {
			get {
				return "Sheriff/Jail/who-is-in-custody.cfm";
			}
			set { 
				_bookingUrlPath=value;
			}
		}

		public WashingtonCountyScraper(County county) : base(county) {

		}

		protected override async Task RunScraperInternal() {
			List<BookingRecord> listRecords=new List<BookingRecord>();
			foreach(string letter in CapitalLetters) {
				//Try to get the listing page.
				string message=await TryGetResponseGET($@"{_countyUrlRoot}/{_bookingUrlPath}/?alpha={letter}");
				if(string.IsNullOrEmpty(message)) {
					//Some error occurred. This was logged already. Just return.
					return;
				}
				HtmlDocument doc=new HtmlDocument();
				doc.LoadHtml(message);
				HtmlNodeCollection inmateListItems=doc.DocumentNode.SelectNodes("//ul[@class='inmates']/li");
				if(inmateListItems==null) {
					//TODO: Log.
					return;
				}
				foreach(HtmlNode inmateOutterNode in inmateListItems) {
					#region Individual Data
					HtmlNode individual=inmateOutterNode.SelectSingleNode("div[@class='individual']/h3");
					if(individual==null) {
						//TODO: Log.
						continue;
					}
					string innerText=individual.InnerText??"";
					string[] names=innerText.Split(',',StringSplitOptions.RemoveEmptyEntries);
					if(names.Length < 2) {
						//TODO: Log.
						continue;
					}
					string[] firstMiddleNames=names[1].Split(' ',StringSplitOptions.RemoveEmptyEntries);
					BookingRecord record=new BookingRecord();
					//No mugshots in Washington County
					record.MugshotImageLink="";
					record.IsMugshotValid=false;
					record.FName=firstMiddleNames[0].Capitalize();
					record.MName=firstMiddleNames.Length > 1 ? firstMiddleNames[1].Capitalize() : "";
					record.LName=names[0].Capitalize();
					#endregion
					#region Physical Data
					HtmlNode physicalInmateData=inmateOutterNode.SelectSingleNode("div[@class='inmate-data']/dl[@class='physical']");
					if(physicalInmateData==null) {
						//TODO: Log.
						continue;
					}
					//Physical data is in pairs.
					List<HtmlNode> listPhysicalNodes=physicalInmateData.ChildNodes.Where(x => x.NodeType!=HtmlNodeType.Text).ToList();
					for(int i=0;i<listPhysicalNodes.Count;i+=2) {
						string innerPhysicalText= listPhysicalNodes[i].InnerText?.ToLower()??"";
						if(innerPhysicalText.Contains("sex")) {
							record.Gender=ParseUtils.ParseGender(listPhysicalNodes[i+1].InnerText);
						}
						else if(innerPhysicalText.Contains("race")) {
							record.Race=ParseUtils.ParseRace(listPhysicalNodes[i+1].InnerText);
						}
						else if(innerPhysicalText.Contains("height")) {
							record.Height=ParseUtils.ParseHeight(listPhysicalNodes[i+1].InnerText);
						}
						else if(innerPhysicalText.Contains("weight")) {
							int.TryParse(listPhysicalNodes[i+1].InnerText,out int weight);
							record.Weight=weight;
						}
					}
					#endregion
					#region Identity Data
					HtmlNode identityInmateData=inmateOutterNode.SelectSingleNode("div[@class='inmate-data']/dl[@class='identity']");
					if(identityInmateData==null) {
						//TODO: Log.
						continue;
					}
					List<HtmlNode> listIdentityNodes=identityInmateData.ChildNodes.Where(x => x.NodeType!=HtmlNodeType.Text).ToList();
					//All we want from this is the date of birth.
					for(int i=0;i<listIdentityNodes.Count;i+=2) {
						string innerIdentityText=listIdentityNodes[i].InnerText?.ToLower()??"";
						if(innerIdentityText.Contains("dob")) {
							DateTime.TryParse(listIdentityNodes[i+1].InnerText,out DateTime dob);
							record.DateOfBirth=dob;
							break;
						}
					}
					#endregion
					#region Charges
					HtmlNode chargeTable=inmateOutterNode.SelectSingleNode("table[@class='booking-data']/tbody");
					if(chargeTable==null) {
						//TODO: Log.
						continue;
					}
					//Foreach charge in the tables
					foreach(HtmlNode chargeNode in chargeTable.ChildNodes.Where(x => x.NodeType!=HtmlNodeType.Text)) {
						List<HtmlNode> listChargeNodeChildren=chargeNode.ChildNodes.Where(x => x.NodeType!=HtmlNodeType.Text).ToList();
						if(listChargeNodeChildren.Count!=9) {
							//TODO: Log
							continue;
						}
						string chargeRaw=listChargeNodeChildren[1].InnerText;
						if(!ParseUtils.TryParseCharge(chargeRaw,out ChargeType chargeType)) {
							//DebuggerUtils.BreakIfDebuggerAttached();
							Console.WriteLine(chargeRaw);
							//TODO: Log
							continue;
						}
					}
					#endregion
					listRecords.Add(record);
				}
			}
		}
	}

}
