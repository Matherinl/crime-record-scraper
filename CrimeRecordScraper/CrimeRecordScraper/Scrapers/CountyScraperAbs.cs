﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using CrimeRecordCore.TableTypes;
using CrimeRecordCore.Utilites;
using HtmlAgilityPack;

namespace CrimeRecordScraper.Scrapers {

	///<summary>An abstract class for a county web scaper. Handles many of the tasks such as error handling and logging.</summary>
	public abstract class CountyScraperAbs {

		///<summary>The url to the root page of the county.</summary>
		protected abstract string _countyUrlRoot { get; }
		///<summary>The path of the url after the root url that leads to the booking part of the website.</summary>
		protected abstract string _bookingUrlPath { get; set;}
		///<summary>The cancellation token used for this scraper.</summary>
		protected CancellationTokenSource _cancellationToken { get; }=new CancellationTokenSource();
		///<summary>Indicates if the scraper is currently running.</summary>
		public bool IsScraperRunning { get; private set; }
		///<summary>The county that this scraper is scraping.</summary>
		protected County _county;

		public CountyScraperAbs(County county) {
			_county=county;
		}

		///<summary>Implement the code that the individual county needs to run in this method.</summary>
		protected abstract Task RunScraperInternal();

		///<summary>Runs the scraper for this county. Will handle scraping the web page and inserting the results into the database.</summary>
		public async Task RunScraper() {
			if(IsScraperRunning) {
				throw new ApplicationException($@"Cannot call RunScraper while it is already running.");
			}
			IsScraperRunning=true;
			try {
				await RunScraperInternal();
			}
			finally {
				IsScraperRunning=false;
			}
		}

		///<summary>Stops the scraper if it is running.</summary>
		public void StopScraper() {
			if(IsScraperRunning) {
				_cancellationToken.Cancel();
			}
		}

		///<summary>Attempts to get the HTML from the passed in url. If this webcall fails, this method will handle logging and return null. This method
		///also handles a webcall that succeeds but returns an error such as a 404.</summary>
		protected async Task<string> TryGetResponseGET(string url) {
			HttpResponseMessage message=null;
			try {
				message=await HttpUtils.GetResponseAsync(url,_cancellationToken.Token);
				//Only null if the web call was cancelled.
				if(message==null) {
					//TODO: Log cancelled webcall.
				}
			}
			catch(HttpRequestException httpEx) {
				//TODO: Log httpEx.
			}
			if(message==null) {
				return null;
			}
			//We got something back. Let's log if it was a bad status code.
			if(!message.IsSuccessStatusCode) {
				//TODO: Log bad error code
				//Return null so the caller does not think the message succeeded.
				return null;
			}
			return await message.Content.ReadAsStringAsync();
		}

	}

}
