﻿using System.Threading.Tasks;
using CrimeRecordCore.TableTypes;
using CrimeRecordScraper.Scrapers.Oregon;

namespace CrimeRecordScraper {

	public class Program {

		public static async Task Main(string[] args) {
			WashingtonCountyScraper scraper=new WashingtonCountyScraper(new County { Id=1,Name="Washington",State="OR" });
			await scraper.RunScraper();
		}

	}

}
