﻿using System;
using System.Text.RegularExpressions;
using CrimeRecordCore.Enums;
using CrimeRecordCore.Extensions;
using CrimeRecordCore.Utilites;

namespace CrimeRecordScraper.Utilities {

	///<summary>Contains useful utilities to parse certain data from strings.</summary>
	public static class ParseUtils {

		///<summary>Parses the gender from the given string. If the input is bad, it will return Gender.Other.</summary>
		public static Gender ParseGender(string input) {
			if(string.IsNullOrEmpty(input)) {
				return Gender.Other;
			}
			input=input.ToLower();
			if(input=="m" || input=="male") {
				return Gender.Male;
			}
			if(input=="f" || input=="female") {
				return Gender.Female;
			}
			//Break to inspect input.
			DebuggerUtils.BreakIfDebuggerAttached();
			return Gender.Other;
		}

		///<summary>Parses the race from the given string. If the input is bad, it will return Race.Other.</summary>
		public static Race ParseRace(string input) {
			if(string.IsNullOrEmpty(input)) {
				return Race.Other;
			}
			input=input.ToLower();
			if(input=="w" || input=="white" || input=="caucasian") {
				return Race.White;
			}
			if(input=="b" || input=="black") {
				return Race.Black;
			}
			if(input=="a" || input=="asian") {
				return Race.Asian;
			}
			if(input=="i" || input=="native american" || input=="american indian") {
				return Race.NativeAmerican;
			}
			if(input=="u" || input=="unknown") {
				return Race.Other;
			}
			//Break to inspect input.
			DebuggerUtils.BreakIfDebuggerAttached();
			return Race.Other;
		}

		///<summary>Parses the height from the given string. If the input is bad, it will return 0.</summary>
		public static int ParseHeight(string input) {
			if(string.IsNullOrEmpty(input)) {
				return 0;
			}
			//If its a simple number in inches, we will return that.
			if(int.TryParse(input,out int result)) {
				return result;
			}
			Match match=Regex.Match(input,$@"([0-9])' ([0-9][0-9]?)""");
			if(match==null || match.Groups.Count!=3) {
				DebuggerUtils.BreakIfDebuggerAttached();
				return 0;
			}
			int.TryParse(match.Groups[1].Value,out int feet);
			int.TryParse(match.Groups[2].Value,out int inches);
			return feet*12+inches;
		}

		///<summary>Attempts to parse out the charge from the given input. Will return if it can parse the input or not.</summary>
		public static bool TryParseCharge(string input,out ChargeType charge) {
			charge=ChargeType.Unknown;
			if(string.IsNullOrEmpty(input)) {
				return false;
			}
			input=input.ToLower();
			if(input.Contains("disorderly conduct") || input.ContainsAll("dis","con")) {
				charge=ChargeType.DisorderlyConduct;
			}
			else if(input.ContainsAll("attempt","commit","crime")) {
				charge=ChargeType.AttemptToCommitCrime;
			}
			else if(input.ContainsAll("violation","release","agreement")) {
				charge=ChargeType.ViolationOfReleaseAgreement;
			}
			else if(input.Contains("trespass") || input.ContainsAll("crim","tres")) {
				charge=ChargeType.Trespassing;
			}
			else if(input.ContainsAll("fail","appear")) {
				charge=ChargeType.FailToAppear;
			}
			else if(input.ContainsAll("assault","officer")) { 
				charge=ChargeType.AssaultOfPoliceOfficer;
			}
			else if(input.ContainsAll("encourage","child","sex","abuse")) {
				charge=ChargeType.EncourageChildSexAbuse;
			}
			else if(input.ContainsAll("computer","crime")) {
				charge=ChargeType.ComputerCrime;
			}
			else if(input.ContainsAll("luring","minor")) {
				charge=ChargeType.LuringAMinor;
			}
			else if(input.ContainsAll("aggravated","harassment")) {
				charge=ChargeType.AggravatedHarassment;
			}
			else if(input.ContainsAll("publ","drink")) {
				charge=ChargeType.PublicIntoxication;
			}
			else if(input.Contains("robbery")) {
				charge=ChargeType.Robbery;
			}
			else if(input.Contains("theft")) {
				charge=ChargeType.Theft;
			}
			else if(input.Contains("burglary")) {
				charge=ChargeType.Burglary;
			}
			else if(input.ContainsAll("fraudulent","credit","card")) {
				charge=ChargeType.FraudulentUseOfACreditCard;
			}
			else if(input.Contains("fugitive")) {
				charge=ChargeType.Fugitive;
			}
			else if(input.Contains("sexual abuse")) {
				charge=ChargeType.SexualAbuse;
			}
			else if(input.Contains("strangulation")) {
				charge=ChargeType.Strangulation;
			}
			else if(input.Contains("sodomy")) {
				charge=ChargeType.Sodomy;
			}
			else if(input.ContainsAll("interfere","report")) {
				charge=ChargeType.InterfereWithMakingReport;
			}
			else if(input.ContainsAll("aggravated","murder")) {
				charge=ChargeType.AggravatedMurder;
			}
			else if(input.ContainsAll("contempt","court")) {
				charge=ChargeType.ContemptOfCourt;
			}
			else if(input.ContainsAll("sexual","penetration")) {
				charge=ChargeType.SexualPenetration;
			}
			else if(input.ContainsAll("unlawful","distribution") || input.ContainsAll("unlawful","delivery")) {
				charge=ChargeType.UnlawfulDistributionOfControlledSubstance;
			}
			else if(input.ContainsAll("controlled","substance")) {
				charge=ChargeType.ControlledSubstanceOffense;
			}
			else if(input.ContainsAll("liquor","minor")) {
				charge=ChargeType.GivingLiquorToMinor;
			}
			else if(input.ContainsAll("poss","weapon")) {
				charge=ChargeType.PossesionOfAWeapon;
			}
			else if(input.ContainsAll("elude","police")) {
				charge=ChargeType.AttemptToEludePolice;
			}
			else if(input.ContainsAll("tampering","witness")) {
				charge=ChargeType.TamperingWithAWitness;
			}
			else if(input.ContainsAll("unlawful","weapon")) {
				charge=ChargeType.UnlawfulUseOfAWeapon;
			}
			else if(input.ContainsAll("unlawful","purchase","firearm")) {
				charge=ChargeType.UnlawfulPurchaseOfFirearm;
			}
			else if(input.ContainsAll("driving","sus")) {
				charge=ChargeType.DrivingWithSuspendedLicense;
			}
			else if(input.ContainsAll("hit","run")) {
				charge=ChargeType.HitAndRun;
			}
			else if(input.ContainsAll("reckless","driving")) {
				charge=ChargeType.RecklessDriving;
			}
			else if(input.Contains("uumv") || input.ContainsAll("unauthorized","motor","vehicle")) {
				charge=ChargeType.UnautharizedUseOfMotorVehicle;
			}
			else if(input.ContainsAll("posses","stolen","vehicle")) {
				charge=ChargeType.PossesionOfAStolenVehicle;
			}
			else if(input.Contains("coercion")) {
				charge=ChargeType.Coercion;
			}
			else if(input.Contains("duii")) {
				charge=ChargeType.DrivingUnderInfluence;
			}
			else if(input.ContainsAll("false","info")) {
				charge=ChargeType.FalseInfoToPoliceOfficer;
			}
			else if(input.ContainsAll("rest","order")) {
				charge=ChargeType.ViolatingRestrainingOrder;
			}
			else if(input.Contains("criminal mischief")) {
				charge=ChargeType.CriminalMischief;
			}
			else if(input.ContainsAll("interfere","public","transport")) {
				charge=ChargeType.InterfereWithPublicTransportation;
			}
			else if(input.ContainsAll("report","sex") || input.ContainsAll("register","sex")) {
				charge=ChargeType.FailToReportAsSexOffender;
			}
			else if(input.ContainsAll("interfere","officer")) {
				charge=ChargeType.InterfereWithPoliceOfficer;
			}
			else if(input.ContainsAll("public","indecency")) {
				charge=ChargeType.PublicIndecency;
			}
			else if(input.ContainsAll("private","indecency")) {
				charge=ChargeType.PrivateIndecency;
			}
			else if(input.ContainsAll("reckless","endanger")) {
				charge=ChargeType.RecklessEndangerment;
			}
			else if(input.ContainsAll("criminal","mistreatment")) {
				charge=ChargeType.CriminalMistreatment;
			}
			else if(input.Contains("resisting arrest")) {
				charge=ChargeType.ResistingArrest;
			}
			else if(input.Contains("heroin")) {
				charge=ChargeType.Heroin;
			}
			else if(input.Contains("harassment")) {
				charge=ChargeType.Harassment;
			}
			else if(input.Contains("methamphetamine")) {
				charge=ChargeType.Methamphetamine;
			}
			else if(input.Contains("oxycodone")) {
				charge=ChargeType.Oxycodone;
			}
			else if(input.Contains("arson")) {
				charge=ChargeType.Arson;
			}
			else if(input.Contains("racketeering")) {
				charge=ChargeType.Racketeering;
			}
			else if(input.Contains("murder")) {
				charge=ChargeType.Murder;
			}
			else if(input.Contains("escape")) {
				charge=ChargeType.Escape;
			}
			else if(input.Contains("menacing")) {
				charge=ChargeType.Menacing;
			}
			else if(input.Contains("incest")) {
				charge=ChargeType.Incest;
			}
			else if(input.Contains("rape")) {
				charge=ChargeType.Rape;
			}
			else if(input.Contains("forg")) {
				charge=ChargeType.Forgery;
			}
			else if(input.Contains("prostitution") || input.Contains("prostitute")) {
				charge=ChargeType.Prostitution;
			}
			else if(input.Contains("stalk")) {
				charge=ChargeType.Stalking;
			}
			else if(input.Contains("manslaughter")) {
				charge=ChargeType.Manslaughter;
			}
			else if(input.Contains("probation")) {
				charge=ChargeType.ProbationViolation;
			}
			else if(input.Contains("parole")) {
				charge=ChargeType.ParoleViolation;
			}
			else if(input.Contains("assault")) {
				charge=ChargeType.Assault;
			}
			else if(input.Contains("hold")) {
				charge=ChargeType.HoldForCharge;
			}
			else if(input.Contains("kidnapping")) {
				charge=ChargeType.Kidnapping;
			}
			else {
				return false;
			}
			return true;
		}
	}

}
